from player import Player
from position import Position
from ai import Ai
from board import Board
import time



class Game:   

    def render(self):
        self.currentBoard.render()

    def state2(self):
        keepGoing = True

        self.render()
        winning = self.currentBoard.checkWin()
        if(winning):
            self.gameEnded = True
            keepGoing = False
            
        else:
            draw = self.currentBoard.checkDraw()

            if(draw):
                self.gameEnded = True
                keepGoing = False
                print("******************** the game ended, no one wins ***************")

        return keepGoing
                              
    def masterHandler(self):
        # pvp
        if(self.gameType == "pvp"):
            whichTurn = 1

            while(self.gameEnded == False):
                # state 1 - one player's turn
                print("pick a number for where you want to place your pice\n(don't you fucking pick one in use, I AM NOT CHECKING FOR THAT. you are not funny):")
                userInput = input()
                if(whichTurn == 1):
                    self.currentBoard.playerMove(userInput, self.p1)
                else:
                    self.currentBoard.playerMove(userInput, self.p2)

                # state 2 - check game-win, then render (if yes, end it)
                self.state2()  
                if(self.gameEnded == True):
                    break        
                
                # state 3 - change player/pc, for the next turn
                if(whichTurn == 2):
                    whichTurn = 1
                else:
                    whichTurn = 2

        # pvc
        elif(self.gameType == "pvc"):
            whichTurn = 1

            while(self.gameEnded == False):
                # state 1 - one player's / AI's turn
                if(whichTurn == 1):
                    print("pick a number for where you want to place your pice\n(don't you fucking pick one in use, I AM NOT CHECKING FOR THAT. you are not funny):")
                    userInput = input()
                    self.currentBoard.playerMove(userInput, self.p1)
                else:
                    aiInput = self.currentBoard.aiThinking(self.p2)
                    self.currentBoard.aiMove(aiInput, self.p2)


                # state 2 - check game-win, then render (if yes, end it)
                self.state2()   
                if(self.gameEnded == True):
                    break       

                # state 3 - change player/pc, for the next turn
                if(whichTurn == 2):
                    whichTurn = 1
                else:
                    whichTurn = 2
                
        # cvc        
        elif(self.gameType == "cvc"): 
            whichTurn = 1

            while(self.gameEnded == False):
                # state 1 - pc / pc turn
                if(whichTurn == 1):
                    aiInput = self.currentBoard.aiThinking(self.p1)
                    self.currentBoard.aiMove(aiInput, self.p1)
                else:
                    aiInput = self.currentBoard.aiThinking(self.p2)
                    self.currentBoard.aiMove(aiInput, self.p2)

                # state 2 - check game-win, then render (if yes, end it)
                self.state2()
                if(self.gameEnded == True):
                    break

                # this line is only called, when they didn't win and the full grid is full
                if(len(self.currentBoard.stack) == 9):
                    self.gameEnded = True
                    print("******************** the game ended, no one wins ***************")
                    break

                # state 3 - change player/pc, for the next turn
                if(whichTurn == 2):
                    whichTurn = 1
                else:
                    whichTurn = 2

    def __init__(self, gameType, boardInput):
        self.gameType = gameType
        self.p1 = Player("o")
        self.p2 = Player("x")
        self.gameEnded = False

        if(boardInput == ""):
            self.currentBoard = Board("")
        else:
            self.currentBoard = boardInput