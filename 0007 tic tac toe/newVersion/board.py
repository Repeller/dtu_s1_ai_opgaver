from player import Player
from position import Position
from ai import Ai

class Board:   
    def makeBoard(self):
        map = []

        # location
        # 1, 2, 3
        # 4, 5, 6
        # 7, 8, 9

        # list value 
        # 0, 1, 2 
        # 3, 4, 5
        # 6, 7, 8

        # all types
        # v1, v2, v3, h1, h2, h3, c1, c2

        # top
        map.append(Position(1, " ", [[0,3,6], [0,1,2], [0,4,8]]))
        map.append(Position(2, " ", [[1,4,7], [0,1,2]]))
        map.append(Position(3, " ", [[2,5,8], [0,1,2], [2,4,6]]))
        # mid
        map.append(Position(4, " ", [[0,3,6], [3,4,5]]))
        map.append(Position(5, " ", [[1,4,7], [3,4,5], [0,4,8], [2,4,6]]))
        map.append(Position(6, " ", [[2,5,8], [3,4,5]]))
        # low
        map.append(Position(7, " ", [[0,3,6], [6,7,8], [2,4,6]]))
        map.append(Position(8, " ", [[1,4,7], [6,7,8]]))
        map.append(Position(9, " ", [[2,5,8], [6,7,8], [0,4,8]]))

        return map

    def render(self):
        counter = 0
        output = ""

        print("-------------")
        print("values               keys    ")
        for x in self.currentBoard:
            counter = counter + 1
            output = output + "|" +  x.value 

            # end of line 1
            if(counter == 3):
                output = output + "|             |1|2|3|    \n"
            # end of line 2
            if(counter == 6):
                output = output + "|             |4|5|6|    \n"
            # end of line 3
            if(counter == 9):
                output = output + "|             |7|8|9|    "
        
        
        print(output)
        print("-------------")

    def playerMove(self, location, player):
        # if(location.isnumeric or int(location) < 0):
        #     print("you didn't give me a usable number...... | " + locatcurrentBoard not a usable number... game over!")
        #     input()

        numb = int(location)

        # this should be patch at some point, but not before it have been showed in the class first xDDDDDDDD
        if(int(numb) in self.stack):
            print("this spot is in use, your turn have bethisBoard..... funny right xD")
            #self.funnyGuy()

        # if the move can be made
        else:
            print("playerMove -------- setting a new value to the grid", numb)

            # [index, symbol]
            self.stack.append([numb-1, player.symbol])

            self.currentBoard[numb-1].value = player.symbol
    
    def aiMove(self, location, player):
        numb = int(location)
        self.stack.append(numb)
        self.currentBoard[numb-1].value = player.symbol

    def setMove(self, location, symbol):
        playerSymbol = lambda symbol:"o" if symbol == True else "x"
        self.stack.append(location)
        self.currentBoard[location-1].value = playerSymbol

    def aiThinking(self, player):
        print("the ai is thinking .......... 'beep' 'boop' ")

        spots = self.getEmptySpots()
        symbol = lambda player : True if player.symbol == "o" else False
        

        out = self.mind.minimax("", self, 3, -999999, 999999, symbol)
        print(type(out))
        print(out)
        
        
        

        if(symbol):
            max_value = max(moveValues)
            max_index = moveValues.index(max_value)
            return(moves[max_index])
        else:
            min_value = min(moveValues)
            min_index = moveValues.index(min_value)
            return(moves[min_index])

    def funnyGuy(self):
        print("we got a funny guy here...")
        
        for x in range(1, 30):
            if(x % 30 == 0):
                print("!!!!!!!!!!!!!!! GO GET HIM BOYS !!!!!!!!!!!!!!")
            elif(x % 3 == 0 and x % 2 == 0):
                print("I told you not to do that.................... you have tied my hands")
            elif(x % 3 == 0):
                print("why would you do that.... ****!, **** YOU !")
            elif(x % 2 == 0):
                print("I'm sure I told you not to do that..... I said you shouldn't do that......... why do make me do this")
        
        print(". . . . . .")
        time.sleep(100)

    def getEmptySpots(self):
        output = []
        for x in self.currentBoard:
            if(x.value == " "):
                output.append(x)

        return output

    def getAllLine(self):
        output_list = []

        output_list.append(["v1", [0,3,6]])
        output_list.append(["v2", [1,4,7]])
        output_list.append(["v3", [2,5,8]])
        output_list.append(["h1", [0,1,2]])
        output_list.append(["h2", [3,4,5]])
        output_list.append(["h3", [6,7,8]])
        output_list.append(["c1", [0,4,8]])
        output_list.append(["c2", [2,4,6]])
        
        return output_list

    def checkWin(self):
        
        winning = False
        #winningType = "nothing"
        
        allLines = self.getAllLine()


        for line in allLines:
            symbol = ""
            counter = 0

            for x in line[1]:
                # this line don't mean anything, skip it
                if(self.currentBoard[x].value == " "):
                    break
                else:
                    # first run
                    if(symbol == ""):
                        symbol = self.currentBoard[x].value
                    
                    if(symbol == self.currentBoard[x].value):
                        counter = counter + 1
                    
                if(counter == 3):
                    winning = True
                    self.gameEnded = True
                    print("******************** player '" + symbol +"' have won the game ***************")
                    #winningType = line[0]
                    break
            
            if(winning):
                break
        
        return winning

    def checkDraw(self):
        counter = 0

        for x in self.currentBoard:
            if x.value != " ":
                counter += 1
        
        if(counter == 9):
            print("******************** we got no winners here, it is a draw, better luck next time ***************")
            return True
        else:
            return False


    def __init__(self, board):
        self.stack = []
        self.mind = Ai()

        if(board == ""):
            self.currentBoard = self.makeBoard()
        else:
            self.currentBoard = board

    def __str__(self):
        return "current board: " +  str(self.currentBoard)