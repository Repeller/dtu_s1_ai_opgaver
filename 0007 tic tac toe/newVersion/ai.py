from position import Position


class Ai:

    def checkOneLine(self, position, curBoard, maximizingPlayer):
        
        def playerSymbol(
            maximizingPlayer): return "o" if maximizingPlayer else "x"

        # there is always 1, unless we are doung "c"/cross, then there can be 2

        # 3,2,3,
        # 2,4,2,
        # 3,2,3

        baseValue = [3, 2, 3, 2, 4, 2, 3, 2, 3]

        output = 0

        print("x ----------:", position)
        enemy = 0
        mine = 0
        empty = 0

        for win in position.possibleWins:
            print("win ----------:", win)
            for cor in win:
                # print("cor 1 ----------:", cor)
                # print("cor 2 ----------:", curBoard[cor])
                # print("cor 3 ----------:", curBoard[cor].value)

                outputValue = 0

                # checking all 3 spots
                if(curBoard[cor].value == " "):
                    # outputValue = outputValue - 2
                    empty = empty + 1
                    outputValue = outputValue + baseValue[cor]
                elif(curBoard[cor].value == playerSymbol):
                    outputValue = outputValue + 4
                    mine = mine + 1
                    outputValue = outputValue + baseValue[cor]
                else:
                    outputValue = outputValue - 9
                    enemy = enemy + 1
                    outputValue = outputValue + baseValue[cor]

                # this is a win
                if(mine == 2 and empty == 1):
                    outputValue = outputValue + 30

                output += outputValue
                    
                outputValue = 0
                empty = 0
                mine = 0
                enemy = 0

        return [position.location, output]

    def checkLines(self, position, curBoard, maximizingPlayer):
        outputword = ""
        outputValue = 0

        # there is always 1, unless we are doung "c"/cross, then there can be 2

        # 3,2,3,
        # 2,4,2,
        # 3,2,3

        baseValue = [3, 2, 3, 2, 4, 2, 3, 2, 3]

        pointsFromEveryLine = []

        for pos in position:
            q = self.checkOneLine(pos, curBoard, maximizingPlayer)
            pointsFromEveryLine(q)


        output = 0
        maxIndex, maxV = max(pointsFromEveryLine, key=lambda item: item[1])

        
        return pointsFromEveryLine[maxIndex]

    # position
    def minimax(self, position, thisBoard, depth, alpha, beta, maximizingPlayer):
        firstTime = False

        listOfMoves = ""
        if(position == ""):
            # first time it gets called
            firstTime = True
            listOfMoves = thisBoard.getEmptySpots()
        else:
            listOfMoves = position

        tempBoard = thisBoard

        # end - after one instance have been done with the 'minimax' function
        if(depth == 0):
            print("depth 0 - return - ***********************************")
            return self.checkLines(position, tempBoard.currentBoard, maximizingPlayer)
        elif(True):
            # here we check, if the possiable moves, are lover than the depth
            print("")
        elif(True):
            # is the map full now, draw or not, then we should stop the minimax
            print("")

        # start -

        if(maximizingPlayer):
            maxEval = -999999
            #print("max - ***********************************")

            for child in listOfMoves:
                tempBoard.setMove(child.location, True)
                tempListOverMoves = listOfMoves
                tempListOverMoves.pop(tempListOverMoves.index(child))

                evalValue = self.minimax(
                    tempListOverMoves, tempBoard, depth-1, alpha, beta, False)
                maxEval = max(maxEval, evalValue)
                alpha = max(alpha, evalValue)
                if(beta <= alpha):
                    break
                return maxEval
        else:
            minEval = 999999
            #print("min - ***********************************")
            for child in position:
                tempBoard.setMove(child.location, False)
                tempListOverMoves = listOfMoves
                tempListOverMoves.pop(tempListOverMoves.index(child))

                evalValue = self.minimax(
                    tempListOverMoves, tempBoard, depth-1, alpha, beta, True)
                minEval = min(minEval, evalValue)
                beta = min(minEval, evalValue)
                if(beta <= alpha):
                    break

            print("end of MinMax -------****************************----:", minEval)
            return minEval
