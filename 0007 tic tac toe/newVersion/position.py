
class Position:
    def __init__(self, location, value, wins):
        self.location = location
        self.value = value
        self.possibleWins = wins

    def __str__(self):
        return "Location: " + str(self.location) + " | value: " + str(self.value) + " | possibleWins: " + str(self.possibleWins)

