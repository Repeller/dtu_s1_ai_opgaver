from game import Game
from board import Board

print("first you need to pick a game type")
print("1: pvp (player vs player)")
print("2: pvc (player vs computer)")
print("3: cvc (computer vs computer)")
print("pick 1,2,3 by writing one of those 3 numbers:")

gameTypeInput = input()

playerCount = 0
handler = ""

# pvp
if(gameTypeInput == "1"):
    print("you picked pvp")
    handler = Game("pvp", Board(""))
    playerCount = 2

# pvc
if(gameTypeInput == "2"):
    print("you picked pvc")
    handler = Game("pvc", Board(""))
    playerCount = 1
    
# cvc
if(gameTypeInput == "3"):
    print("you picked cvc")
    handler = Game("cvc", Board(""))
    playerCount = 0

handler.render()

handler.masterHandler()
