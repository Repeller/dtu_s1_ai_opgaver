from game import Game 
from player import Player

playerOneTurn = True


grid = [
    [' ',' ',' '],
    [' ',' ',' '],
    [' ',' ',' '],
]

pointMap = [
    ['3','2','3'],
    ['2','4','2'],
    ['3','2','3'],
]
# the first 3 arrays are horizentel 
# then 3 vertical 
# then the 2 cross
winList = [
    [ [0,0], [0,1], [0,2] ] ,
    [ [1,0], [1,1], [1,2] ] ,
    [ [2,0], [2,1], [2,2] ] ,

    [ [0,0], [1,0], [2,0] ] ,
    [ [0,1], [1,1], [2,1] ] ,
    [ [0,2], [1,2], [2,2] ] ,

    [ [0,0], [1,1], [2,2] ] ,
    [ [0,2], [1,1], [0,0] ] 
     ]


# get all the moves, sorted by the highest value
def getCoordinates():
    a1 = 0
    a2 = 0

    converted = []
    for i in pointMap:
        
        for x in i:
            converted.append([x,[a1,a2]])
            a2 += 1
        
        a2 = 0
        a1 += 1
    
    converted.sort(reverse = True)
    return converted

def render(gridImg):
    #os.system('clear')
    for x in gridImg:
        temp = ""
        for y in x:
            if(y == " "):
                temp += "_" + " "
            else:
                temp += y + " "
        print(temp)
        #print(" ")

def makeMove(move):

    p1 = move[1][0]
    p2 = move[1][1]
    if(grid[p1][p2] == " "):
        # here we place the icon from the player
        value = ""
        if(playerOneTurn == True):
            value = "x"
        else:
            value = "o"
        
        grid[p1][p2] = value
        stack.append([p1,p2])
        return True
    else:
        return False


def search(moves):
    moved = False

    for m in moves:
        p1 = m[1][0]
        p2 = m[1][1]
        if(grid[p1][p2] == " "):
            
            # here we place the icon from the player
            input = ""
            if(playerOneTurn == True):
                input = "x"
            else:
                input = "o"
            

            grid[p1][p2] = input
            moved = True
            stack.append([p1,p2])
            break
    
    if(moved):
        return True
    else:
        return False

render(grid)
print("-------------")

# stack = []

# output = getCoordinates()

# #print(output)

# movesLeft = True
# moves = getCoordinates()

# for x in moves:
#     makeMove(x)
#     playerOneTurn = not playerOneTurn


# render(grid)
# print("-------------")
# render(pointMap)

# print(stack)
    
print("first you need to pick a game type")
print("1: pvp (player vs player)")
print("2: pvc (player vs computer)")
print("3: cvc (computer vs computer)")
print("pick 1,2,3 by writing one of those 3 numbers:")

gameType = input()

# pvp
if(gameType == "1"):
    print("you picked pvp")
    handler = Game("pvp")

# pvc
if(gameType == "2"):
    print("you picked pvc")
    handler = Game("pvc")
    
# cvc
if(gameType == "3"):
    print("you picked cvc")
    handler = Game("cvc")

    