from player import Player

class Game:
    def __init__(self, gameType):
        self.type = gameType
        
        if(gameType == "pvp"):
            self.p1 = Player("o", "p")
            self.p2 = Player("x", "p")
        if(gameType == "pvc"):
            self.p1 = Player("o", "p")
            self.p2 = Player("x", "c")
        if(gameType == "cvc"):
            self.p1 = Player("o", "c")
            self.p2 = Player("x", "c")

