print("you need to write 2 numbers, that will check, give the first number now:")

def biggestNum(a,b):
    output = ""

    if(a==b):
        output = a + " and " + b + " have the same value"
    if(a>b):
        output = a + " is bigger than " + b
    if(b>a):
        output = b + " is bigger than " + a

    return output

userValue1 = input()
print("you said a = ", userValue1, "now give a number for b:")
userValue2 = input()
print("you said b = ", userValue2)

out = biggestNum(userValue1, userValue2)
print("out:", out)