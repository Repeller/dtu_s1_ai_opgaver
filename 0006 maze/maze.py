import os

mazeWithWalls = [
    ['*','*','*','*','*','*','*','*','*','*','*','*','*'],
    ['*',' ','*',' ','*',' ','*',' ',' ',' ',' ',' ','*'],
    ['*',' ','*',' ',' ',' ','*',' ','*','*','*',' ','*'],
    ['*',' ',' ','S','*','*','*',' ',' ',' ',' ',' ','*'],
    ['*',' ','*',' ',' ',' ',' ',' ','*','*','*',' ','*'],
    ['*',' ','*',' ','*','*','*',' ','*',' ',' ',' ','*'],
    ['*',' ','*',' ','*',' ',' ',' ','*','*','*',' ','*'],
    ['*',' ','*',' ','*','*','*',' ','*',' ','*',' ','*'],
    ['*',' ',' ',' ',' ',' ',' ',' ',' ',' ','*','G','*'],
    ['*','*','*','*','*','*','*','*','*','*','*','*','*']
]


maze = [
    [' ','*',' ','*',' ','*',' ',' ',' ',' ',' '],
    [' ','*',' ',' ',' ','*',' ','*','*','*',' '],
    [' ',' ','S','*','*','*',' ',' ',' ',' ',' '],
    [' ','*',' ',' ',' ',' ',' ','*','*','*',' '],
    [' ','*',' ','*','*','*',' ','*',' ',' ',' '],
    [' ','*',' ','*',' ',' ',' ','*','*','*',' '],
    [' ','*',' ','*','*','*',' ','*',' ','*',' '],
    [' ',' ',' ',' ',' ',' ',' ',' ',' ','*','G'],
]

def render(mazeImg):
    os.system('clear')
    for x in mazeImg:
        temp = ""
        for y in x:
            temp += y + " "
        print(temp)
        print(" ")

render(mazeWithWalls)

# start from S
# look for G - which will be the end
# keep track of every step

stack = []


def search(s):
    stack.append(s)

    if mazeWithWalls[s[0]][s[1]] == "G":
        return True

    if mazeWithWalls[s[0]][s[1]] == "*" or mazeWithWalls[s[0]][s[1]] == "V":
        stack.pop()
        return False
    
    mazeWithWalls[s[0]][s[1]] = "V"

    # go up
    up = search([s[0]-1, s[1]])
    if up:
        return True

    # go right
    right = search([s[0], s[1]+1])
    if right:
        return True

    # go down 
    down = search([s[0]+1, s[1]])
    if down:
        return True

    # go left
    left = search([s[0], s[1]-1])
    if left:
        return True

    stack.pop()

def doStuff():

    # (y,x)
    start = [3,3]
    end = [8,11]
    print("start", start, "|", "end", end)

    # 000 rule - if spot is 'v', then ignore
    # 001 rule - go up
    # 002 rule - then go right
    # 003 rule - then go down
    # 004 rule - then go left
    # 005 rule - then start over

    search(start)


doStuff()

# printing my stack
print("path", stack)

print("this is how the map looks like now")
render(mazeWithWalls)

