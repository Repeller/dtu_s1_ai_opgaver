
# this function is recursive 
# https://da.wikipedia.org/wiki/Rekursion
# En rekursion betegner noget, der refererer til sig selv.
def fib(n):
    if(n < 2):
        return n

    else:
        return (fib(n-2) + fib(n-1))


print("insert the number of N, to get the Fibonacci number for that N:")

userBase = input()

# for i in range(1, int(userBase)+1):
#     print(i, ":", fib(i))

print("you said: " + userBase)

print(  fib( int(userBase) ) )
