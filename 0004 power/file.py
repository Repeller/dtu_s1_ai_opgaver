def power(base, value):
    print("start of Power, (base, value)", base, value)
    tempV = int(value)
    tempB = int(base)
    out = 0
    first = True
    count = 1

    while count < tempV:
        if(first):
            print("first")

            out += tempB * tempB
            print("out", out)
            first = False


        else:
            print("not first")
            number = out * tempB
            print("number", number)

            out = number
            print("new out", out)

        count += 1
        
        print("--------------------")
    
    return out


print("insert 2 numbers for Power, first is the base:")

userBase = input()

print("you said: " + userBase)
print("now give the exponent: ")


userExponent = input()

print("you said: " + userExponent)

output = power(userBase, userExponent)
print("output is: ", str(output))