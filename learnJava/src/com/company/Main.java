package com.company;

// java for c# devs
// https://youtu.be/heZJ3iGj3KA

enum Device {
    PHONE(200, 800), TABLET(400, 1200), LAPTOP(1800, 1200), DESKTOP(8000, 4000);

    public int ScreenWidth;
    public int ScreenHeight;

    Device(int screenWidth, int screenHeight){
        ScreenWidth = screenWidth;
        ScreenHeight = screenHeight;
    }
}

// java har ikke props, som man kender fra C¤
class C  {
    // private field
    String _str;

    // dette er "accessor" method'en

    String getStr() {
        return _str;
    }

    // dette er mutator method'en
    // hvis vi kun vil have et variable, som aldrig skal ændres (only get), så skriver man bare ikke "setter" metoden
    void setStr(String str) {
        _str = str;
    }
}

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("hello world");

        Device d = Device.TABLET;

        System.out.println("H:" + d.ScreenHeight + " W: " + d.ScreenWidth);

    }
}
